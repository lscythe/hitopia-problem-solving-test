# HITOPIA - Problem Solving Test

### 1. [Weighted Strings](src/WeightedString.kt)
You can try it here :[Kotlin Playground](https://pl.kotl.in/sBgTg8Zyb)

### 2. [Balanced Bracket](src/BalancedBracket.kt)
Complexity time for this code is O(n), the code iterates through each character in the input string once,
resulting in a linear time complexity of O(n), where n is the length of the input string.

You can try it here :[Kotlin Playground](https://pl.kotl.in/3VQXmTelX)

### 3. [HighestPalindrome](src/HighestPalindrome.kt)
You can try it here :[Kotlin Playground](https://pl.kotl.in/KA80e7YWl)
