fun main() {
    val s = "{ ( ( [ ] ) [ ] ) [ ] }"

    println(isBalanced(s))
}

fun isBalanced(input: String): String {
    val stack = ArrayDeque<Char>()
    val brackets = mapOf('(' to ')', '[' to ']', '{' to '}')

    input.forEach { char ->
        if (brackets.containsKey(char)) {
            stack.addFirst(char)
        } else if (brackets.values.contains(char)) {
            if (stack.isEmpty()) {
                return "NO"
            }
            val top = stack.removeFirst()
            if (char != brackets[top]) {
                return "NO"
            }
        }
    }

    return if (stack.isNotEmpty()) "NO" else "YES"
}
