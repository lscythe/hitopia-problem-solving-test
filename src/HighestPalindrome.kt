fun main() {
    println(highestPalindrome("3943", 1))
    println(highestPalindrome("932239", 2))
}

fun highestPalindrome(s: String, k: Int): String {
    val n = s.length
    val arr = s.toCharArray()
    var changesNeeded = 0

    val toChange = BooleanArray(n) { false }

    fun makePalindrome(i: Int): Int {
        if (i >= n / 2) return 0
        if (arr[i] != arr[n - i - 1]) {
            changesNeeded++
            toChange[i] = true
            toChange[n - i - 1] = true
        }
        return makePalindrome(i + 1)
    }

    makePalindrome(0)

    if (changesNeeded > k) return "-1"

    var changesLeft = k - changesNeeded

    fun maximizePalindrome(i: Int): String {
        if (i >= n / 2) return String(arr)
        if (arr[i] != arr[n - i - 1]) {
            val maxDigit = maxOf(arr[i], arr[n - i - 1])
            arr[i] = maxDigit
            arr[n - i - 1] = maxDigit
        }
        if (changesLeft > 0) {
            if (arr[i] != '9') {
                if (toChange[i] || toChange[n - i - 1]) {
                    arr[i] = '9'
                    arr[n - i - 1] = '9'
                    changesLeft--
                } else if (changesLeft >= 2) {
                    arr[i] = '9'
                    arr[n - i - 1] = '9'
                    changesLeft -= 2
                }
            }
        }
        return maximizePalindrome(i + 1)
    }

    val result = maximizePalindrome(0)

    if (n % 2 != 0 && changesLeft > 0) {
        arr[n / 2] = '9'
    }

    return result
}
