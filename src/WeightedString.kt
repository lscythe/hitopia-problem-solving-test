fun main() {
    val s  = "abbcccd"
    val queries = listOf(1,3,9,8)

    println(weightedStrings(s, queries))
}

fun weightedStrings(s: String, queries: List<Int>): List<String> {
    val weights = mutableSetOf<Int>()

    for (i in s.indices) {
        var weight = 0
        for (j in i..<s.length) {
            if (s[i] == s[j]) {
                weight += s[j].code - 'a'.code + 1
                weights.add(weight)
            } else {
                break
            }
        }
    }

    return queries.map { if (it in weights) "Yes" else "No" }
}
